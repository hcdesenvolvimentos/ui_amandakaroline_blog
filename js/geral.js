$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaqueLeft").owlCarousel({
		items : 1,
		dots: true,
		loop: true,
		lazyLoad: true,
		mouseDrag:false,
		touchDrag  : false,	       
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		animateOut: 'fadeOut',
		smartSpeed: 450,			    		   		    
	    
	});
		
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaqueRight").owlCarousel({
		items : 1,
		dots: true,
		loop: true,
		lazyLoad: true,
		mouseDrag:false,
		touchDrag  : false,	       
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		animateOut: 'fadeOut',
		smartSpeed: 450,		    		   		    
	    
	});
	
	//CARROSSEL DE DESTAQUE
	$("#carrosselCategoria").owlCarousel({
		items : 3,
		dots: true,
		loop: true,
		lazyLoad: true,
		mouseDrag:true,
		touchDrag  : true,	       
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		animateOut: 'fadeOut',
		smartSpeed: 450,	

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:3
            },
            			            
        }		    		   		    
	    
	});

	//SCRIPTS PÁGINA DE POST'S
	// FUNCAO PARA OCULTAR/MOSTRAR AS OPCOES DE COMPARTILHAMENTO
	$('.compartilhar').hide();
	$(window).bind('scroll', function () {
       var alturaScroll = $(window).scrollTop();
        if(alturaScroll > 100 ){
            $('.compartilhar').show("slow");
            setTimeout(function(){
             	$('.compartilhar small').show("slow");  
         	}, 2000);
            
        }else{

            $('.compartilhar').hide();
        }
    });

    //FUNÇÃO DE CLIQUE PARA APERECER AS REDES SOCIAIS	
	$("#click").click(function() {
	  $("#mostre").fadeToggle("slow", "linear");
	});


	// SCRIPT PARA MOSTRAR O MENU FIXO APÓS O SCROLL
	$(window).bind('scroll', function () {
       
       var alturaScroll = $(window).scrollTop()
       if (alturaScroll > 100) {
            $(".menu-fixo").fadeIn();
       }else{
            $(".menu-fixo").fadeOut();

       }
    });

    // SCRIPT PARA MOSTRAR AS REDES SOCIAIS APOS O SCROLL
	$(window).bind('scroll', function () {
       
       var alturaScroll = $(window).scrollTop()
       if (alturaScroll > 100 && alturaScroll < 1250) {
            $(".redesFixas").fadeIn();
       }else{
            $(".redesFixas").fadeOut();

       }
    });
});